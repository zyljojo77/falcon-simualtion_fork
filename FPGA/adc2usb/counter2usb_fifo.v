`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    13:23:47 11/15/2013 
// Design Name: 
// Module Name:    adc2usb
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
// oLed<0> flaga
// oLed<1> flagb
// oLed<2> flagc; // full
// oLed<3> flagd; // empty
// oLed<4> 1'b0; 
// oLed<5> 1'b0;//dbug_sig;
// oLed<6> 1'b0;
// oLed<7> ofifo_full;
//
//////////////////////////////////////////////////////////////////////////////////


//`define CHANGE_ENDIANESS


module adc2usb(
	// For USB
	input 				reset_n,
	input 				ifclk,
	inout		[15 : 0] fdata,  
	output 	[1  : 0]	faddr,  
	output slrd,   
	output slwr,   
	output sloe,              
	input flagd,  
	input flaga,
	input flagb,
	input flagc,
	output pkt_end,
	output dbug_sig,
	output [7:0] oLed,
	input oUsbRstN,
	input iClk100Mhz,
	
	/*
	 * ADC
	 */
	input				EXP2_ADS_CLKOUT,
	input				EXP2_ADS_CLKOUTM,

	input	[6	: 0]	EXP2_ADS_DA_P,
	input	[6	: 0]	EXP2_ADS_DA_M,
	input	[6	: 0]	EXP2_ADS_DB_P,
	input	[6	: 0]	EXP2_ADS_DB_M
);

/* Internal Paramentors for USB */
reg slrd_n;
reg slwr_n;
reg sloe_n;
reg slrd_d_n;

/*
 * Internal Paramentors for ADC
 */
reg 	[13	: 0]	adc_data_i;
reg 	[13	: 0]	adc_data_q;
reg 	[15	: 0]	data_i;
reg 	[15	: 0]	data_q;
	
wire 	[6	: 0]	adc_data_i_temp;
wire 	[6	: 0]	adc_data_i_temp_inv;
wire 	[6	: 0]	adc_data_q_temp;
wire 	[6	: 0]	adc_data_q_temp_inv;
wire  			adc_data_clock;
wire  			adc_data_clock_inv;

wire				fifoReadEnable;

parameter stream_in_idle   = 1'b0;
parameter stream_in_write  = 1'b1;

reg current_stream_in_state;
reg next_stream_in_state;

//dbugging 
reg flagd_d;
reg slwr_n_d;

// Paramentors for counter
parameter   		pCounterWidth = 32;
parameter   		pFrencencyDiv = 25;
// Internal paramentors
wire 		[pCounterWidth - 1 : 0] oCounter;
wire 		oDiv;
wire 		ofifo_counter_div;
wire 		[pCounterWidth - 1 : 0] ofifo_Counter;
wire 		ofifo_full;
wire 		[15:0] ofifo_output;
wire		fifoEmpty;
reg		FifoWriteEnable = 0;
wire     wFifoAlmostFull;

`ifdef CHANGE_ENDIANESS
	assign fdata ={change_endian(ofifo_output[15:8]), change_endian(ofifo_output[7:0])};
`else
	assign fdata ={ofifo_output[15:8], ofifo_output[7:0]};
`endif

assign slwr = slwr_n;
assign slrd = slrd_n;
assign sloe = sloe_n;
assign pkt_end = 1'b1;
assign faddr 	= 2'b00; // EP6
assign oLed[0] = flaga;
assign oLed[1] = flagb; // full
assign oLed[2] = flagc; // empty
assign oLed[3] = flagd;
assign oLed[4] = 1'b0; 
assign oLed[5] = oDiv;//dbug_sig;
assign oLed[6] = reset_n;
assign oLed[7] = ofifo_full;
assign fifoReadEnable = (!fifoEmpty) & flagb;

IBUFDS_DIFF_OUT IBUFDS_INST0 (
	.O (adc_data_clock), 				// Buffer ouput
	.OB (adc_data_clock_inv),
	.I (EXP2_ADS_CLKOUT), 				// diff input, p-channel
	.IB(EXP2_ADS_CLKOUTM) 				// diff input, n-channel
);

// 3x reversing polarity in ucf, data input and data output
IBUFDS_DIFF_OUT IBUFDS_INST1 [6:0] (
	.O (adc_data_i_temp_inv), 			// ADC I Data Buffer ouput
	.OB (adc_data_i_temp),
	.I (EXP2_ADS_DA_M), 					// diff input, p-channel
	.IB(EXP2_ADS_DA_P) 		    		// diff input, n-channel
);

// 3x reversing polarity in ucf, data input and data output
IBUFDS_DIFF_OUT IBUFDS_INST2 [6:0] (
	.O (adc_data_q_temp_inv), 			// ADC Q Data Buffer ouput
	.OB (adc_data_q_temp),
	.I (EXP2_ADS_DB_M),					// diff input, p-channel
	.IB(EXP2_ADS_DB_P) 					// diff input, n-channel
);
	
counter_n #(.pCounterWidth(pCounterWidth),
				.pFrencencyDiv(pFrencencyDiv)) counter (
 .iClk(adc_data_clock), 
 .iReset(!reset_n), 
 .iEnable(1'b1), 
 .oCounter(), 
 .oDiv(oDiv)
 );
 
//counter_n #(.pCounterWidth(pCounterWidth),
//				.pFrencencyDiv(pFrencencyDiv)) fifo_counter (
// .iClk(iClk100Mhz), 
// .iReset(!reset_n), 
// .iEnable(1'b1), 
// .oCounter(ofifo_Counter), 
// .oDiv(ofifo_counter_div)
// ); 

// instantiate FIFO memory
fifo_32x16 fifo (
	.rst(!reset_n), 				// input rst
	.wr_clk(adc_data_clock), 	// input clock from ADC
	.din({data_i, data_q}), 	// input [31 : 0] din from ADC
//	.din(data_i), 					// input [15 : 0] din from ADC
	.wr_en(FifoWriteEnable), 		// input wr_en !ofifo_full
	.full(ofifo_full), 			// output full
	.almost_full(wFifoAlmostFull), 				// output almost_full

	.rd_clk(ifclk), 				// input rd_clk
	.dout(ofifo_output), 		// output [15 : 0] dout
	.rd_en(fifoReadEnable), 				// input rd_en fifoReadEnable
	.empty(fifoEmpty), 			// output empty
	.almost_empty() 				// output almost_empty
);

// instantiate debouncers
//debouncer debouncer_inst1 (
//debouncer #(
//	.COUNTER_LEN(24),
//	.COUNTER_EVAL_LEN(20)
//) debouncer_inst1 (
////	.clk(iClk100Mhz),
//	.clk(adc_data_clock),
//	.button(wTrigger),
//	.single_shot_out(wTriggerDebounced)
//);

always @ (posedge adc_data_clock)		
	begin 
		adc_data_i[0] 	<= adc_data_i_temp[0];
		adc_data_i[2] 	<= adc_data_i_temp[1];
		adc_data_i[4] 	<= adc_data_i_temp[2];
		adc_data_i[6] 	<= adc_data_i_temp[3];
		adc_data_i[8] 	<= adc_data_i_temp[4];
		adc_data_i[10] <= adc_data_i_temp[5];
		adc_data_i[12] <= adc_data_i_temp[6];
		
		adc_data_q[0] 	<= adc_data_q_temp[0];
		adc_data_q[2] 	<= adc_data_q_temp[1];
		adc_data_q[4] 	<= adc_data_q_temp[2];
		adc_data_q[6] 	<= adc_data_q_temp[3];
		adc_data_q[8] 	<= adc_data_q_temp[4];
		adc_data_q[10] <= adc_data_q_temp[5];
		adc_data_q[12] <= adc_data_q_temp[6];
	end
	
always @ (negedge adc_data_clock)
	begin
		adc_data_i[1] 	<= adc_data_i_temp[0];
		adc_data_i[3] 	<= adc_data_i_temp[1];
		adc_data_i[5] 	<= adc_data_i_temp[2];
		adc_data_i[7] 	<= adc_data_i_temp[3];
		adc_data_i[9] 	<= adc_data_i_temp[4];
		adc_data_i[11] <= adc_data_i_temp[5];
		adc_data_i[13] <= adc_data_i_temp[6];
		
		adc_data_q[1] 	<= adc_data_q_temp[0];
		adc_data_q[3] 	<= adc_data_q_temp[1];
		adc_data_q[5] 	<= adc_data_q_temp[2];
		adc_data_q[7] 	<= adc_data_q_temp[3];
		adc_data_q[9] 	<= adc_data_q_temp[4];
		adc_data_q[11] <= adc_data_q_temp[5];
		adc_data_q[13] <= adc_data_q_temp[6];
	end
	
always @ (posedge adc_data_clock)		
	begin
		data_i[15:0] <= {1'b0, 1'b0, adc_data_i[13:0]};
		data_q[15:0] <= {1'b0, 1'b0, adc_data_q[13:0]};
//		data_i[15:0] <= {adc_data_i[13], adc_data_i[13], adc_data_i[13:0]};
//		data_q[15:0] <= {adc_data_q[13], adc_data_q[13], adc_data_q[13:0]};
	end

//write control signal generation
always@(*)begin
	if((current_stream_in_state == stream_in_write) & (flagb == 1'b1))
		slwr_n <= 1'b0;
	else
		slwr_n <= 1'b1;
end

always@(posedge ifclk, negedge reset_n) begin
	if(reset_n == 1'b0)begin
		flagd_d  <= 1'b0;
		slwr_n_d <= 1'b1;
 	end else begin
		flagd_d  <= flagd_d;
		slwr_n_d <= slwr_n;
	end
end

always @ (posedge adc_data_clock) begin
	if (!wFifoAlmostFull)
			FifoWriteEnable	<= 1;
	else
			FifoWriteEnable	<= 0;
end

assign dbug_sig = flagd_d | slwr_n_d;

//Stream IN mode state machine 
always@(posedge ifclk, negedge reset_n) begin
	if(reset_n == 1'b0)
      		current_stream_in_state <= stream_in_idle;
        else
            current_stream_in_state <= next_stream_in_state;
end

//Stream IN mode state machine combo
always@(*) begin
	next_stream_in_state = current_stream_in_state;
	case(current_stream_in_state)
		stream_in_idle:begin
			//if((flagd == 1'b1) & (sync == 1'b1))
			if(flagb == 1'b1)
				next_stream_in_state = stream_in_write;
			else
				next_stream_in_state = stream_in_idle;
		end
		stream_in_write:begin
			if(flagb == 1'b0)
				next_stream_in_state = stream_in_idle;
			else
				next_stream_in_state = stream_in_write;
		end
		default: 
			next_stream_in_state = stream_in_idle;
	endcase
end


	/*
	 * FUNCTION
	 */
	function [7 : 0] change_endian(input [7 : 0] dataIn);
		integer k;
		begin
			for (k=0; k<8; k=k+1)
				change_endian[k] = dataIn[7-k];
		end
	endfunction	

endmodule