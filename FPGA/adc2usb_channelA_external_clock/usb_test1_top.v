`timescale 1ns / 1ps

module adc2usb1_top(
	
	/*
	 * ADC
	 */
	input				EXP2_ADS_CLKOUT,
	input				EXP2_ADS_CLKOUTM,

	input	[6	: 0]	EXP2_ADS_DA_P,
	input	[6	: 0]	EXP2_ADS_DA_M,
	input	[6	: 0]	EXP2_ADS_DB_P,
	input	[6	: 0]	EXP2_ADS_DB_M, 
	
	/*
	 * DSP
	 */
	//input				iClk100Mhz,
	output	[7	: 0]	oLed,
//	input	[2	: 0]	iPushButtonN,
	input	[7	: 0]	iDipSwitchN,

	output	[15	: 0]	ioUsbData,
	input					iUsbFlagALvl,
	input					iUsbFlagBFullN,
	input					iUsbFlagCEmptyN,
	output				oUsbSlRdN,		// slave-FIFO read
	output				oUsbSlWrN,		// slave-FIFO write
	output				iUsbIFClk,		// optional external clock
	output				oUsbSlOEN,		// slave-FIFO output enable
	output	[1	: 0]	oUsbFifoAddr,	// slave-FIFO address select
	output				oUsbPktEndN,	// slave-FIFO packet end
	output				oUsbSlCSN,		// slave-FIFO enable
	output				oUsbRstN,	// USB device active-low reset
	input 				iResetN,
	input 				iUsbClkOUT,
	
		/* Test Signal */
	output oADC_DATA_CLOCK,
	output oUSB_IFCLOCK
	);


	/*
	 * ADC
	 */
	reg 	[13	: 0]	adc_data_i;
	reg 	[13	: 0]	adc_data_q;
	reg 	[15	: 0]	data_i;
	reg 	[15	: 0]	data_q;
		
	wire 	[6	: 0]	adc_data_i_temp;
	wire 	[6	: 0]	adc_data_i_temp_inv;
	wire 	[6	: 0]	adc_data_q_temp;
	wire 	[6	: 0]	adc_data_q_temp_inv;
	wire  				adc_data_clock;
	wire  				adc_data_clock_inv;	
	
	
	/*
	 * DSP
	 */
	wire				wUsbWriteEvent;
	
	wire				wFifoFull;
	wire				wFifoAlmostFull;
	wire				wFifoEmpty;
	wire				wFifoAlmostEmpty;
	
	wire				wFifoReadEnable;
	reg					rFifoOutputValid;
	
	wire				wSystemReset;
	
	//reg		[15	: 0]	rCounter = 0;
	reg					rFifoWriteEnable = 0;
	
	wire	[15	: 0]	wFifoDataOut;
//	wire				wTrigger;
//	wire				wTriggerDebounced;
//	wire				wPktEnd;
//	wire				wPktEndDebounced;
	wire 	iFIFO_WE_M;
	
//	assign iFIFO_WE_M  = iDipSwitchN[7];

	/* ADC ADS62P49 differential clock inputs */
	IBUFGDS_DIFF_OUT #(
		.DIFF_TERM("TRUE"), // Differential Termination
		.IBUF_LOW_PWR("FLASE"),
		.IOSTANDARD("LVDS_25") // Specify the input I/O standard
	) IBUFGDS_INST0 (
		.O (adc_data_clock), 		// Buffer ouput
		.OB (adc_data_clock_inv),
		.I (EXP2_ADS_CLKOUT), 		// diff input, p-channel
		.IB(EXP2_ADS_CLKOUTM) 		// diff input, n-channel
	);
	
	/* ADC ADS62P49 differential data inputs */
	// 3x reversing polarity in ucf, data input and data output
	IBUFDS_DIFF_OUT #(
		.DIFF_TERM("TRUE"), 			// Differential Termination
		.IBUF_LOW_PWR("FLASE"),
		.IOSTANDARD("LVDS_25") 		// Specify the input I/O standard
	) IBUFDS_INST1 [6:0] (
		.O (adc_data_i_temp_inv), 	// ADC I Data Buffer ouput
		.OB (adc_data_i_temp),
		.I (EXP2_ADS_DA_M), 			// diff input, p-channel
		.IB(EXP2_ADS_DA_P) 			// diff input, n-channel
	);
	
	// 3x reversing polarity in ucf, data input and data output
	IBUFDS_DIFF_OUT #(
		.DIFF_TERM("TRUE"), 			// Differential Termination
		.IBUF_LOW_PWR("FLASE"),
		.IOSTANDARD("LVDS_25") 		// Specify the input I/O standard
	) IBUFDS_INST2 [6:0] (
		.O (adc_data_q_temp_inv), 	// ADC Q Data Buffer ouput
		.OB (adc_data_q_temp),
		.I (EXP2_ADS_DB_M),			// diff input, p-channel
		.IB(EXP2_ADS_DB_P) 			// diff input, n-channel
	);

	// instantiate USB handling module
	usb_handling usb_handling_inst (
		.oUsbData(ioUsbData), 
		.oUsbReadN(oUsbSlRdN), 
		.oUsbWriteN(oUsbSlWrN), 
		.oUsbOutputEnableN(oUsbSlOEN), 
		.oUsbFifoAddr(oUsbFifoAddr), 
		.oUsbChipSelectN(oUsbSlCSN), 
		.iWriteEvent(wUsbWriteEvent), 
		.iData(wFifoDataOut)
    );
	
	// instantiate FIFO memory
	fifo fifo_inst (
		.rst(!iResetN), // input rst
		
		//.wr_clk(iClk100Mhz), // input wr_clk
		.wr_clk(adc_data_clock), // input wr_clk
		//.din(rCounter), // input [15 : 0] din
		.din(data_i), // input [15 : 0] din {data_i, data_q}
		//.din(data_i), // input [15 : 0] din
//		.din({18'b00_0000_0000, data_q}), 
		.wr_en(iFIFO_WE_M), // input wr_en  rFifoWriteEnable
		
		.full(wFifoFull), // output full
		.almost_full(wFifoAlmostFull), // output almost_full

		.rd_clk(iUsbIFClk), // input rd_clk
		.dout(wFifoDataOut), // output [15 : 0] dout
		.rd_en(wFifoReadEnable), // input rd_en
		.empty(wFifoEmpty), // output empty
		.almost_empty(wFifoAlmostEmpty) // output almost_empty
	);

	FIFO_FSM fifo_write_ctrl (
		 .iClk(adc_data_clock), 
		 .iRst_N(iResetN), 
		 .iEmpty(wFifoAlmostEmpty), 
		 .iFull(wFifoAlmostFull), 
		 .oFIFO_WR_EN(iFIFO_WE_M)
		 );
	
	assign wUsbWriteEvent	= rFifoOutputValid;
	assign oUsbRstN			= 1'bz;//!wSystemReset;
	
//	assign wSystemReset 	= !iPushButtonN[0];
//	assign wTrigger			= !iPushButtonN[1];
//	assign wPktEnd			= !iPushButtonN[2];
	
//	assign oUsbPktEndN		= !wPktEndDebounced;//!iPushButtonN[2];
	
	// Output the ADC clock
//	assign oADC_DATA_CLOCK  = adc_data_clock;
	assign oADC_DATA_CLOCK  = iUsbClkOUT;
	assign oUSB_IFCLOCK 		= iUsbIFClk;
	assign iUsbIFClk			= adc_data_clock;
	
	assign oLed[0]			= wFifoEmpty;
	assign oLed[1]			= wFifoAlmostEmpty;		
	assign oLed[2]			= wFifoFull;	
	assign oLed[3]			= wFifoAlmostFull;	
	assign oLed[4]			= wFifoReadEnable;
	assign oLed[5]			= !iUsbFlagCEmptyN;
	assign oLed[6]			= !iUsbFlagBFullN;
	assign oLed[7]			= !iResetN;
	
	always @ (posedge adc_data_clock)		
		begin 
			adc_data_i[0] 	<= adc_data_i_temp[0];
			adc_data_i[2] 	<= adc_data_i_temp[1];
			adc_data_i[4] 	<= adc_data_i_temp[2];
			adc_data_i[6] 	<= adc_data_i_temp[3];
			adc_data_i[8] 	<= adc_data_i_temp[4];
			adc_data_i[10]  <= adc_data_i_temp[5];
			adc_data_i[12]  <= adc_data_i_temp[6];
			
//			adc_data_q[0] 	<= adc_data_q_temp[0];
//			adc_data_q[2] 	<= adc_data_q_temp[1];
//			adc_data_q[4] 	<= adc_data_q_temp[2];
//			adc_data_q[6] 	<= adc_data_q_temp[3];
//			adc_data_q[8] 	<= adc_data_q_temp[4];
//			adc_data_q[10]  <= adc_data_q_temp[5];
//			adc_data_q[12]  <= adc_data_q_temp[6];
		end
		
	always @ (negedge adc_data_clock)
		begin
			adc_data_i[1] 		<= adc_data_i_temp[0];
			adc_data_i[3] 		<= adc_data_i_temp[1];
			adc_data_i[5] 		<= adc_data_i_temp[2];
			adc_data_i[7] 		<= adc_data_i_temp[3];
			adc_data_i[9] 		<= adc_data_i_temp[4];
			adc_data_i[11]  	<= adc_data_i_temp[5];
			adc_data_i[13]  	<= adc_data_i_temp[6];
			
//			adc_data_q[1] 	<= adc_data_q_temp[0];
//			adc_data_q[3] 	<= adc_data_q_temp[1];
//			adc_data_q[5] 	<= adc_data_q_temp[2];
//			adc_data_q[7] 	<= adc_data_q_temp[3];
//			adc_data_q[9] 	<= adc_data_q_temp[4];
//			adc_data_q[11]  <= adc_data_q_temp[5];
//			adc_data_q[13]  <= adc_data_q_temp[6];
		end
		
	always @ (posedge adc_data_clock)		
		begin
			data_i[15:0] <= {adc_data_i[13], adc_data_i[13], adc_data_i[13:0]}; // converation of 2 complement
//			data_q[15:0] <= {adc_data_q[13], adc_data_q[13], adc_data_q[13:0]};
//			data_i[15:0] <= {1'b0, 1'b0, adc_data_i[13:0]};
//			data_q[15:0] <= {1'b0, 1'b0, adc_data_q[13:0]};			
		end
	
	assign wFifoReadEnable = (!wFifoAlmostEmpty) & iUsbFlagBFullN;
//	assign wFifoReadEnable = iUsbFlagBFullN;
	
	always @ (posedge iUsbIFClk)
		begin
			rFifoOutputValid	<= wFifoReadEnable;
		end

endmodule
