clc;
clear all;
close all;

% USB verification

fid1 = fopen('C:\Dokumente und Einstellungen\guillaume\Desktop\dvbt2\pc\usb\usb_data_sink_v2\binary\data37.dump', 'rb');
% fid1  = fopen('C:\Dokumente und Einstellungen\guillaume\Desktop\verilog\ddc1\test3.dump', 'rb');
% fid2  = fopen('C:\Dokumente und Einstellungen\guillaume\Desktop\verilog\ddc1\ascii2.dump', 'r');
% fid2  = fopen('C:\Dokumente und Einstellungen\guillaume\Desktop\verilog\ddc1\signal.dump', 'r');

bitlength = 16;

data1		= fread(fid1, Inf, 'int16');
% temp		= fscanf(fid2, '%s');
% data2_temp	= reshape(temp, bitlength, [])';
% data_dec	= bin2dec(data2_temp);

% value_count = size(data_dec,1);

% % for idx = 1:value_count
% % 	data_extend(idx,:) = data_dec(idx,1)*ones(1,36-bitlength);
% % end
% % 
% % data_extend = [data_extend data_dec];
% 
% for idx = 1:value_count
% 	if (data_dec(idx) < 2^(bitlength-1))
% 		data2(idx,1) = data_dec(idx);
% 	else
% 		data2(idx,1) = -2^bitlength + data_dec(idx);
% 	end
% end

fclose(fid1);
% fclose(fid2);

data_i = data1(1:2:end);
data_q = data1(2:2:end);
% data_p = data2(1:2:end);

% N  = length(data_i);
% fa = 100e6;
% f = (0:1:N-1)/N*fa-fa/2;
% 
% win_hanning = hann(N);
% 
% hanning_data_i = data_i.*win_hanning;
% hanning_data_q = data_q.*win_hanning;
% 
% fft_hanning_data_i = fft(hanning_data_i);
% fft_hanning_data_q = fft(hanning_data_q);

% delta_i   = diff(data_i);
% delta_q   = diff(data_q);
% err_i     = (delta_i ~= 1);
% err_q     = (delta_q ~= 1);

% num_errors_i = sum(err_i)
% num_errors_q = sum(err_q)
% err_loc_i = find(err_i);
% err_loc_q = find(err_q);
% loc_diff_i = diff(err_loc_i);
% loc_diff_q = diff(err_loc_q);

figure;
subplot(2,1,1)
plot(data_i);
grid on;
subplot(2,1,2)
plot(data_q);
grid on;
hold on;
box on;

% figure;
% plot(data2);
% grid on;
% hold on;
% box on;

% figure;
% subplot(2,1,1)
% plot(f,fftshift(20*log10(abs(fft_hanning_data_i))));
% grid on;
% subplot(2,1,2)
% plot(f,fftshift(20*log10(abs(fft_hanning_data_q))));
% grid on;
% hold on;
% box on;
