/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Users/xue liu/Desktop/spectrum_sensing/falcon4/module/ADC_ADS62P49_module/ADC_ADS62P49_module.v";
static int ng1[] = {0, 0};
static int ng2[] = {2, 0};
static int ng3[] = {4, 0};
static int ng4[] = {6, 0};
static int ng5[] = {8, 0};
static int ng6[] = {10, 0};
static int ng7[] = {12, 0};
static int ng8[] = {1, 0};
static int ng9[] = {3, 0};
static int ng10[] = {5, 0};
static int ng11[] = {7, 0};
static int ng12[] = {9, 0};
static int ng13[] = {11, 0};
static int ng14[] = {13, 0};



static void Always_118_0(char *t0)
{
    char t6[8];
    char t15[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    char *t14;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    unsigned int t21;
    int t22;

LAB0:    t1 = (t0 + 4608U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(118, ng0);
    t2 = (t0 + 5920);
    *((int *)t2) = 1;
    t3 = (t0 + 4640);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(119, ng0);

LAB5:    xsi_set_current_line(120, ng0);
    t4 = (t0 + 2648U);
    t5 = *((char **)t4);
    memset(t6, 0, 8);
    t4 = (t6 + 4);
    t7 = (t5 + 4);
    t8 = *((unsigned int *)t5);
    t9 = (t8 >> 0);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t7);
    t12 = (t11 >> 0);
    t13 = (t12 & 1);
    *((unsigned int *)t4) = t13;
    t14 = (t0 + 3208);
    t16 = (t0 + 3208);
    t17 = (t16 + 72U);
    t18 = *((char **)t17);
    t19 = ((char*)((ng1)));
    xsi_vlog_generic_convert_bit_index(t15, t18, 2, t19, 32, 1);
    t20 = (t15 + 4);
    t21 = *((unsigned int *)t20);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB6;

LAB7:    xsi_set_current_line(121, ng0);
    t2 = (t0 + 2648U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 1);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 1);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3208);
    t7 = (t0 + 3208);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng2)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB8;

LAB9:    xsi_set_current_line(122, ng0);
    t2 = (t0 + 2648U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 2);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 2);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3208);
    t7 = (t0 + 3208);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng3)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB10;

LAB11:    xsi_set_current_line(123, ng0);
    t2 = (t0 + 2648U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 3);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 3);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3208);
    t7 = (t0 + 3208);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng4)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB12;

LAB13:    xsi_set_current_line(124, ng0);
    t2 = (t0 + 2648U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 4);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 4);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3208);
    t7 = (t0 + 3208);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng5)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB14;

LAB15:    xsi_set_current_line(125, ng0);
    t2 = (t0 + 2648U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 5);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 5);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3208);
    t7 = (t0 + 3208);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng6)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB16;

LAB17:    xsi_set_current_line(126, ng0);
    t2 = (t0 + 2648U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 6);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 6);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3208);
    t7 = (t0 + 3208);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng7)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB18;

LAB19:    xsi_set_current_line(128, ng0);
    t2 = (t0 + 2808U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 0);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 0);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3368);
    t7 = (t0 + 3368);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng1)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB20;

LAB21:    xsi_set_current_line(129, ng0);
    t2 = (t0 + 2808U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 1);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 1);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3368);
    t7 = (t0 + 3368);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng2)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB22;

LAB23:    xsi_set_current_line(130, ng0);
    t2 = (t0 + 2808U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 2);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 2);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3368);
    t7 = (t0 + 3368);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng3)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB24;

LAB25:    xsi_set_current_line(131, ng0);
    t2 = (t0 + 2808U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 3);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 3);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3368);
    t7 = (t0 + 3368);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng4)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB26;

LAB27:    xsi_set_current_line(132, ng0);
    t2 = (t0 + 2808U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 4);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 4);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3368);
    t7 = (t0 + 3368);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng5)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB28;

LAB29:    xsi_set_current_line(133, ng0);
    t2 = (t0 + 2808U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 5);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 5);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3368);
    t7 = (t0 + 3368);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng6)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB30;

LAB31:    xsi_set_current_line(134, ng0);
    t2 = (t0 + 2808U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 6);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 6);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3368);
    t7 = (t0 + 3368);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng7)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB32;

LAB33:    goto LAB2;

LAB6:    xsi_vlogvar_wait_assign_value(t14, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB7;

LAB8:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB9;

LAB10:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB11;

LAB12:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB13;

LAB14:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB15;

LAB16:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB17;

LAB18:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB19;

LAB20:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB21;

LAB22:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB23;

LAB24:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB25;

LAB26:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB27;

LAB28:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB29;

LAB30:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB31;

LAB32:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB33;

}

static void Always_137_1(char *t0)
{
    char t6[8];
    char t15[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    char *t14;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    unsigned int t21;
    int t22;

LAB0:    t1 = (t0 + 4856U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(137, ng0);
    t2 = (t0 + 5936);
    *((int *)t2) = 1;
    t3 = (t0 + 4888);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(138, ng0);

LAB5:    xsi_set_current_line(139, ng0);
    t4 = (t0 + 2648U);
    t5 = *((char **)t4);
    memset(t6, 0, 8);
    t4 = (t6 + 4);
    t7 = (t5 + 4);
    t8 = *((unsigned int *)t5);
    t9 = (t8 >> 0);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t7);
    t12 = (t11 >> 0);
    t13 = (t12 & 1);
    *((unsigned int *)t4) = t13;
    t14 = (t0 + 3208);
    t16 = (t0 + 3208);
    t17 = (t16 + 72U);
    t18 = *((char **)t17);
    t19 = ((char*)((ng8)));
    xsi_vlog_generic_convert_bit_index(t15, t18, 2, t19, 32, 1);
    t20 = (t15 + 4);
    t21 = *((unsigned int *)t20);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB6;

LAB7:    xsi_set_current_line(140, ng0);
    t2 = (t0 + 2648U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 1);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 1);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3208);
    t7 = (t0 + 3208);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng9)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB8;

LAB9:    xsi_set_current_line(141, ng0);
    t2 = (t0 + 2648U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 2);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 2);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3208);
    t7 = (t0 + 3208);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng10)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB10;

LAB11:    xsi_set_current_line(142, ng0);
    t2 = (t0 + 2648U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 3);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 3);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3208);
    t7 = (t0 + 3208);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng11)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB12;

LAB13:    xsi_set_current_line(143, ng0);
    t2 = (t0 + 2648U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 4);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 4);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3208);
    t7 = (t0 + 3208);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng12)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB14;

LAB15:    xsi_set_current_line(144, ng0);
    t2 = (t0 + 2648U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 5);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 5);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3208);
    t7 = (t0 + 3208);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng13)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB16;

LAB17:    xsi_set_current_line(145, ng0);
    t2 = (t0 + 2648U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 6);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 6);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3208);
    t7 = (t0 + 3208);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng14)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB18;

LAB19:    xsi_set_current_line(147, ng0);
    t2 = (t0 + 2808U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 0);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 0);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3368);
    t7 = (t0 + 3368);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng8)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB20;

LAB21:    xsi_set_current_line(148, ng0);
    t2 = (t0 + 2808U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 1);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 1);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3368);
    t7 = (t0 + 3368);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng9)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB22;

LAB23:    xsi_set_current_line(149, ng0);
    t2 = (t0 + 2808U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 2);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 2);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3368);
    t7 = (t0 + 3368);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng10)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB24;

LAB25:    xsi_set_current_line(150, ng0);
    t2 = (t0 + 2808U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 3);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 3);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3368);
    t7 = (t0 + 3368);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng11)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB26;

LAB27:    xsi_set_current_line(151, ng0);
    t2 = (t0 + 2808U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 4);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 4);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3368);
    t7 = (t0 + 3368);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng12)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB28;

LAB29:    xsi_set_current_line(152, ng0);
    t2 = (t0 + 2808U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 5);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 5);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3368);
    t7 = (t0 + 3368);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng13)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB30;

LAB31:    xsi_set_current_line(153, ng0);
    t2 = (t0 + 2808U);
    t3 = *((char **)t2);
    memset(t6, 0, 8);
    t2 = (t6 + 4);
    t4 = (t3 + 4);
    t8 = *((unsigned int *)t3);
    t9 = (t8 >> 6);
    t10 = (t9 & 1);
    *((unsigned int *)t6) = t10;
    t11 = *((unsigned int *)t4);
    t12 = (t11 >> 6);
    t13 = (t12 & 1);
    *((unsigned int *)t2) = t13;
    t5 = (t0 + 3368);
    t7 = (t0 + 3368);
    t14 = (t7 + 72U);
    t16 = *((char **)t14);
    t17 = ((char*)((ng14)));
    xsi_vlog_generic_convert_bit_index(t15, t16, 2, t17, 32, 1);
    t18 = (t15 + 4);
    t21 = *((unsigned int *)t18);
    t22 = (!(t21));
    if (t22 == 1)
        goto LAB32;

LAB33:    goto LAB2;

LAB6:    xsi_vlogvar_wait_assign_value(t14, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB7;

LAB8:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB9;

LAB10:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB11;

LAB12:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB13;

LAB14:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB15;

LAB16:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB17;

LAB18:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB19;

LAB20:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB21;

LAB22:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB23;

LAB24:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB25;

LAB26:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB27;

LAB28:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB29;

LAB30:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB31;

LAB32:    xsi_vlogvar_wait_assign_value(t5, t6, 0, *((unsigned int *)t15), 1, 0LL);
    goto LAB33;

}

static void Always_156_2(char *t0)
{
    char t4[8];
    char t5[8];
    char t20[8];
    char t32[8];
    char *t1;
    char *t2;
    char *t3;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    char *t17;
    char *t18;
    char *t19;
    char *t21;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    char *t29;
    char *t30;
    char *t31;
    char *t33;
    char *t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    char *t41;

LAB0:    t1 = (t0 + 5104U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(156, ng0);
    t2 = (t0 + 5952);
    *((int *)t2) = 1;
    t3 = (t0 + 5136);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(157, ng0);

LAB5:    xsi_set_current_line(158, ng0);
    t6 = (t0 + 3208);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memset(t5, 0, 8);
    t9 = (t5 + 4);
    t10 = (t8 + 4);
    t11 = *((unsigned int *)t8);
    t12 = (t11 >> 0);
    *((unsigned int *)t5) = t12;
    t13 = *((unsigned int *)t10);
    t14 = (t13 >> 0);
    *((unsigned int *)t9) = t14;
    t15 = *((unsigned int *)t5);
    *((unsigned int *)t5) = (t15 & 16383U);
    t16 = *((unsigned int *)t9);
    *((unsigned int *)t9) = (t16 & 16383U);
    t17 = (t0 + 3208);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    memset(t20, 0, 8);
    t21 = (t20 + 4);
    t22 = (t19 + 4);
    t23 = *((unsigned int *)t19);
    t24 = (t23 >> 13);
    t25 = (t24 & 1);
    *((unsigned int *)t20) = t25;
    t26 = *((unsigned int *)t22);
    t27 = (t26 >> 13);
    t28 = (t27 & 1);
    *((unsigned int *)t21) = t28;
    t29 = (t0 + 3208);
    t30 = (t29 + 56U);
    t31 = *((char **)t30);
    memset(t32, 0, 8);
    t33 = (t32 + 4);
    t34 = (t31 + 4);
    t35 = *((unsigned int *)t31);
    t36 = (t35 >> 13);
    t37 = (t36 & 1);
    *((unsigned int *)t32) = t37;
    t38 = *((unsigned int *)t34);
    t39 = (t38 >> 13);
    t40 = (t39 & 1);
    *((unsigned int *)t33) = t40;
    xsi_vlogtype_concat(t4, 16, 16, 3U, t32, 1, t20, 1, t5, 14);
    t41 = (t0 + 3528);
    xsi_vlogvar_wait_assign_value(t41, t4, 0, 0, 16, 0LL);
    xsi_set_current_line(159, ng0);
    t2 = (t0 + 3368);
    t3 = (t2 + 56U);
    t6 = *((char **)t3);
    memset(t5, 0, 8);
    t7 = (t5 + 4);
    t8 = (t6 + 4);
    t11 = *((unsigned int *)t6);
    t12 = (t11 >> 0);
    *((unsigned int *)t5) = t12;
    t13 = *((unsigned int *)t8);
    t14 = (t13 >> 0);
    *((unsigned int *)t7) = t14;
    t15 = *((unsigned int *)t5);
    *((unsigned int *)t5) = (t15 & 16383U);
    t16 = *((unsigned int *)t7);
    *((unsigned int *)t7) = (t16 & 16383U);
    t9 = (t0 + 3368);
    t10 = (t9 + 56U);
    t17 = *((char **)t10);
    memset(t20, 0, 8);
    t18 = (t20 + 4);
    t19 = (t17 + 4);
    t23 = *((unsigned int *)t17);
    t24 = (t23 >> 13);
    t25 = (t24 & 1);
    *((unsigned int *)t20) = t25;
    t26 = *((unsigned int *)t19);
    t27 = (t26 >> 13);
    t28 = (t27 & 1);
    *((unsigned int *)t18) = t28;
    t21 = (t0 + 3368);
    t22 = (t21 + 56U);
    t29 = *((char **)t22);
    memset(t32, 0, 8);
    t30 = (t32 + 4);
    t31 = (t29 + 4);
    t35 = *((unsigned int *)t29);
    t36 = (t35 >> 13);
    t37 = (t36 & 1);
    *((unsigned int *)t32) = t37;
    t38 = *((unsigned int *)t31);
    t39 = (t38 >> 13);
    t40 = (t39 & 1);
    *((unsigned int *)t30) = t40;
    xsi_vlogtype_concat(t4, 16, 16, 3U, t32, 1, t20, 1, t5, 14);
    t33 = (t0 + 3688);
    xsi_vlogvar_wait_assign_value(t33, t4, 0, 0, 16, 0LL);
    goto LAB2;

}

static void Cont_165_3(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;

LAB0:    t1 = (t0 + 5352U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(165, ng0);
    t2 = (t0 + 3528);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t0 + 6064);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memset(t9, 0, 8);
    t10 = 65535U;
    t11 = t10;
    t12 = (t4 + 4);
    t13 = *((unsigned int *)t4);
    t10 = (t10 & t13);
    t14 = *((unsigned int *)t12);
    t11 = (t11 & t14);
    t15 = (t9 + 4);
    t16 = *((unsigned int *)t9);
    *((unsigned int *)t9) = (t16 | t10);
    t17 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t17 | t11);
    xsi_driver_vfirst_trans(t5, 0, 15);
    t18 = (t0 + 5968);
    *((int *)t18) = 1;

LAB1:    return;
}

static void Cont_166_4(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    unsigned int t13;
    unsigned int t14;
    char *t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;

LAB0:    t1 = (t0 + 5600U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(166, ng0);
    t2 = (t0 + 3688);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t0 + 6128);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t9 = *((char **)t8);
    memset(t9, 0, 8);
    t10 = 65535U;
    t11 = t10;
    t12 = (t4 + 4);
    t13 = *((unsigned int *)t4);
    t10 = (t10 & t13);
    t14 = *((unsigned int *)t12);
    t11 = (t11 & t14);
    t15 = (t9 + 4);
    t16 = *((unsigned int *)t9);
    *((unsigned int *)t9) = (t16 | t10);
    t17 = *((unsigned int *)t15);
    *((unsigned int *)t15) = (t17 | t11);
    xsi_driver_vfirst_trans(t5, 0, 15);
    t18 = (t0 + 5984);
    *((int *)t18) = 1;

LAB1:    return;
}


extern void work_m_00000000001646659718_2071956020_init()
{
	static char *pe[] = {(void *)Always_118_0,(void *)Always_137_1,(void *)Always_156_2,(void *)Cont_165_3,(void *)Cont_166_4};
	xsi_register_didat("work_m_00000000001646659718_2071956020", "isim/ADC_ADS62P49_testbench_isim_beh.exe.sim/work/m_00000000001646659718_2071956020.didat");
	xsi_register_executes(pe);
}
